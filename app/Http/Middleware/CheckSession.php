<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Classes\Helper;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->session()->exists('token')) {
            $token = $request->session()->get('token');
            // If no any token in session, redirect back to home page
            return redirect('/');
        }
        if($request->session()->get('token')) {
            $token = $request->session()->get('token');
            if(Helper::validateJWT($token, file_get_contents(base_path().'/public.key'))) {
                return $next($request);
            }
        }
        // default redirect back to home page
        return redirect('/');
    }
}
