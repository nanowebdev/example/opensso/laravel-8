<?php

namespace App\Classes;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Helper {
    /**
     * Encode JWT
     * 
     * @param payload {array}
     * @param privateKey {string}
     * @return string  
     */
    public static function encodeJWT($payload, $privateKey) {
        $jwt = JWT::encode($payload, $privateKey, 'RS256');
        return $jwt;
    }

    /**
     * Decode JWT
     * 
     * @param token {string}
     * @param publicKey {string}
     * @return array
     */
    public static function decodeJWT($token, $publicKey) {
        try {
            JWT::$leeway = 60; // $leeway in seconds
            $decoded = JWT::decode($token, new Key($publicKey, 'RS256'));
            $decoded_array = (array) $decoded;
            return $decoded_array;
        } catch(Exception $e) {
            return [];
        }
    }

    /**
     * Validate JWT
     * 
     * @param token {string}
     * @param publicKey {string}
     * @return bool
     */
    public static function validateJWT($token, $publicKey) {
        try {
            return self::decodeJWT($token, $publicKey);
        } catch (Exception $e) {
            return false;
        }
    }
}
