FROM php:7.3-fpm

WORKDIR /app/laravel-8

RUN apt-get update -y && apt-get install -y git zip unzip
RUN docker-php-ext-install pdo pdo_mysql mbstring
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . ./

RUN composer install

EXPOSE 8000
