<?php

use Illuminate\Support\Facades\Route;
use App\Classes\Helper;
use App\Http\Middleware\CheckSession;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $token = request()->query('token');
    if($token) {
        request()->session()->put('token', $token);
        return redirect('/dashboard');
    } else {
        return view('home');
    }
});

Route::middleware([CheckSession::class])->group(function(){
    Route::get('/dashboard', function () {
        $token = request()->session()->get('token');
        $jwt = Helper::decodeJWT($token,file_get_contents(base_path().'/public.key'));    
        return view('dashboard', ['jwt' => json_decode(json_encode($jwt))]);
    });
    
    Route::get('/logout', function () {
        request()->session()->forget('token');
        request()->session()->flush();
        return redirect('/');
    });
});
