<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signin Using OpenSSO Login Page</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">

        <!-- Example CSS -->
        <link rel="stylesheet" href="{{ asset('/css/example.css') }}">
    </head>
  <body class="text-center">
    
    <div id="login" class="form-signin">
        <h1 class="h3 mb-3 fw-normal">Sign in</h1>
        <hr>
        <!-- YOU MUST CHANGE THE URL LOGIN WITH YOUR OPENSSO -->
        <a href="http://localhost:3000/sso/login/1af99d8c7d704b9783a8e242b6ba04ae" class="w-100 btn btn-lg btn-primary">Single Sign On</a>
    </div>
    </body>
</html>