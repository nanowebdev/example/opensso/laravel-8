<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signin Using OpenSSO Login Page</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">

        <!-- Example CSS -->
        <link rel="stylesheet" href="{{ asset('/css/example.css') }}">
    </head>
    <body class="text-center">
        <div id="user-info" class="form-signin">
            <table class="table">
                <tbody class="text-left">
                    <tr>
                        <th scope="row" colspan="2"><img id="picture" src="<?php echo $jwt->gravatar?>"></th>
                    </tr>
                    <tr>
                        <th scope="row">User ID</th>
                        <td><span><?php echo $jwt->uid?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Username</th>
                        <td><span><?php echo $jwt->unm?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Name</th>
                        <td><span><?php echo $jwt->name?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <td><span><?php echo $jwt->mail?></span></td>
                    </tr>
                </tbody>
            </table>
            <button type="button" class="btn btn-danger" onclick="handleLogout()">Logout</button>
        </div>
        <script>
            function handleLogout() {
                location.href="logout";
            }
        </script>
    </body>
</html>
