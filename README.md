# Laravel 8
This is a simple example to integrate OpenSSO using Laravel 8.

### Concept
The goals is to make users can login via OpenSSO login page using Laravel 8.  
So the flow is:

1. User trying to login via opensso
2. Token send back to home page
3. Save the token to session so we can validate and use the token to every page that require the credentials via middleware.
4. When the token has expired, users logout automatically via middleware.
5. Done.

Why we use session?  
Because using session is the simpler way. For production, better to use redis than session.

### Requirement
- PHP 7.3+
- Laravel 8

### Installation

1. Run composer
```
composer install
```
2. Edit the `resources/views/home.blade.php` at line 20.  
```php
// CHANGE WITH YOUR OPENSSO URL
$url_sso = "http://localhost:3000/sso/login/1af99d8c7d704b9783a8e242b6ba04ae";
```

3. Replace `public.key` and `private.key` from your OpenSSO.  
4. Rename `.env.example` become `.env`
5. Done.


### How to get URL SSO
1. Login to your OpenSSO
2. Go to menu `My SSO`.
3. Add your new SSO.  
![](https://gitlab.com/nanowebdev/example/opensso/laravel-8/-/raw/master/public/img/tutorial-1.png)
4. Now you have `URL SSO` login page.
![](https://gitlab.com/nanowebdev/example/opensso/laravel-8/-/raw/master/public/img/tutorial-2.png)
5. Done.

### How to run the Server
```bash
php artisan serve --port 8000
```

### Credits
- [firebase/php-jwt](https://github.com/firebase/php-jwt).


### Need Help
Just chat with me via Telegram >> [https://t.me/aalfiann](https://t.me/aalfiann).
